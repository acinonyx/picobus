EESchema Schematic File Version 4
LIBS:picobus-deployer-pcb-cache
EELAYER 30 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 1 1
Title ""
Date ""
Rev ""
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
$Comp
L Connector:Screw_Terminal_01x01 J6
U 1 1 5DC00368
P 8000 5000
F 0 "J6" H 8080 5042 50  0000 L CNN
F 1 "Screw_Terminal_01x01" H 8080 4951 50  0000 L CNN
F 2 "lsf-kicad-lib:KEYSTONE-1502-2" H 8000 5000 50  0001 C CNN
F 3 "~" H 8000 5000 50  0001 C CNN
	1    8000 5000
	1    0    0    -1  
$EndComp
$Comp
L Connector:Screw_Terminal_01x01 J7
U 1 1 5DC00BF8
P 8000 5200
F 0 "J7" H 8080 5242 50  0000 L CNN
F 1 "Screw_Terminal_01x01" H 8080 5151 50  0000 L CNN
F 2 "lsf-kicad-lib:KEYSTONE-1502-2" H 8000 5200 50  0001 C CNN
F 3 "~" H 8000 5200 50  0001 C CNN
	1    8000 5200
	1    0    0    -1  
$EndComp
$Comp
L Connector:Screw_Terminal_01x01 J5
U 1 1 5DC00D40
P 6600 5100
F 0 "J5" H 6518 4875 50  0000 C CNN
F 1 "Screw_Terminal_01x01" H 6518 4966 50  0000 C CNN
F 2 "lsf-kicad-lib:KEYSTONE-1502-2" H 6600 5100 50  0001 C CNN
F 3 "~" H 6600 5100 50  0001 C CNN
	1    6600 5100
	-1   0    0    1   
$EndComp
$Comp
L Connector:Screw_Terminal_01x01 J8
U 1 1 5DC00F6A
P 8600 1400
F 0 "J8" H 8680 1442 50  0000 L CNN
F 1 "Screw_Terminal_01x01" H 8680 1351 50  0000 L CNN
F 2 "lsf-kicad-lib:KEYSTONE-1502-2" H 8600 1400 50  0001 C CNN
F 3 "~" H 8600 1400 50  0001 C CNN
	1    8600 1400
	1    0    0    -1  
$EndComp
$Comp
L Connector:Screw_Terminal_01x01 J9
U 1 1 5DC0120D
P 8600 1750
F 0 "J9" H 8680 1792 50  0000 L CNN
F 1 "Screw_Terminal_01x01" H 8680 1701 50  0000 L CNN
F 2 "lsf-kicad-lib:KEYSTONE-1502-2" H 8600 1750 50  0001 C CNN
F 3 "~" H 8600 1750 50  0001 C CNN
	1    8600 1750
	1    0    0    -1  
$EndComp
$Comp
L Connector:Screw_Terminal_01x01 J10
U 1 1 5DC02B11
P 8600 2100
F 0 "J10" H 8680 2142 50  0000 L CNN
F 1 "Screw_Terminal_01x01" H 8680 2051 50  0000 L CNN
F 2 "lsf-kicad-lib:KEYSTONE-1502-2" H 8600 2100 50  0001 C CNN
F 3 "~" H 8600 2100 50  0001 C CNN
	1    8600 2100
	1    0    0    -1  
$EndComp
$Comp
L Connector:Screw_Terminal_01x01 J11
U 1 1 5DC02B17
P 8600 2450
F 0 "J11" H 8680 2492 50  0000 L CNN
F 1 "Screw_Terminal_01x01" H 8680 2401 50  0000 L CNN
F 2 "lsf-kicad-lib:KEYSTONE-1502-2" H 8600 2450 50  0001 C CNN
F 3 "~" H 8600 2450 50  0001 C CNN
	1    8600 2450
	1    0    0    -1  
$EndComp
$Comp
L Connector:Screw_Terminal_01x01 J12
U 1 1 5DC02B1D
P 8600 2800
F 0 "J12" H 8680 2842 50  0000 L CNN
F 1 "Screw_Terminal_01x01" H 8680 2751 50  0000 L CNN
F 2 "lsf-kicad-lib:KEYSTONE-1502-2" H 8600 2800 50  0001 C CNN
F 3 "~" H 8600 2800 50  0001 C CNN
	1    8600 2800
	1    0    0    -1  
$EndComp
$Comp
L Connector:Screw_Terminal_01x01 J13
U 1 1 5DC02B23
P 8600 3150
F 0 "J13" H 8680 3192 50  0000 L CNN
F 1 "Screw_Terminal_01x01" H 8680 3101 50  0000 L CNN
F 2 "lsf-kicad-lib:KEYSTONE-1502-2" H 8600 3150 50  0001 C CNN
F 3 "~" H 8600 3150 50  0001 C CNN
	1    8600 3150
	1    0    0    -1  
$EndComp
$Comp
L Switch:SW_SPDT SW1
U 1 1 5DC02FFF
P 7350 5100
F 0 "SW1" H 7350 5385 50  0000 C CNN
F 1 "SW_SPDT" H 7350 5294 50  0000 C CNN
F 2 "lsf-kicad-lib:D2F-L30-A1" H 7350 5100 50  0001 C CNN
F 3 "~" H 7350 5100 50  0001 C CNN
	1    7350 5100
	1    0    0    -1  
$EndComp
Wire Wire Line
	6800 5100 7150 5100
Wire Wire Line
	7550 5000 7800 5000
Wire Wire Line
	7550 5200 7800 5200
Text Label 6900 5100 0    50   ~ 0
GND
Text Label 7600 5000 0    50   ~ 0
DET_1
Text Label 7600 5200 0    50   ~ 0
DET_2
Wire Wire Line
	8400 1400 8100 1400
Wire Wire Line
	8100 1400 8100 1750
Wire Wire Line
	8100 2100 7600 2100
Wire Wire Line
	8400 1750 8100 1750
Connection ~ 8100 1750
Wire Wire Line
	8100 1750 8100 2100
Wire Wire Line
	8400 2100 8100 2100
Connection ~ 8100 2100
Text Label 7600 2100 0    50   ~ 0
V_DEPLOY
Wire Wire Line
	8400 2450 8100 2450
Wire Wire Line
	8100 2450 8100 2800
Wire Wire Line
	8100 3150 7600 3150
Wire Wire Line
	8400 3150 8100 3150
Connection ~ 8100 3150
Wire Wire Line
	8400 2800 8100 2800
Connection ~ 8100 2800
Wire Wire Line
	8100 2800 8100 3150
Text Label 7600 3150 0    50   ~ 0
GND_D
$Comp
L Connector:Screw_Terminal_01x01 J2
U 1 1 5DC17B73
P 4600 2150
F 0 "J2" H 4680 2192 50  0000 L CNN
F 1 "Screw_Terminal_01x01" H 4680 2101 50  0000 L CNN
F 2 "lsf-kicad-lib:KEYSTONE-7698" H 4600 2150 50  0001 C CNN
F 3 "~" H 4600 2150 50  0001 C CNN
	1    4600 2150
	-1   0    0    1   
$EndComp
$Comp
L Connector:Screw_Terminal_01x01 J1
U 1 1 5DC17FF5
P 4600 1800
F 0 "J1" H 4680 1842 50  0000 L CNN
F 1 "Screw_Terminal_01x01" H 4680 1751 50  0000 L CNN
F 2 "lsf-kicad-lib:KEYSTONE-7698" H 4600 1800 50  0001 C CNN
F 3 "~" H 4600 1800 50  0001 C CNN
	1    4600 1800
	-1   0    0    1   
$EndComp
Wire Wire Line
	4800 1800 5250 1800
Wire Wire Line
	4800 2150 5250 2150
Text Label 5250 1800 2    50   ~ 0
V_DEPLOY
Text Label 5250 2150 2    50   ~ 0
GND_D
$Comp
L Connector:Screw_Terminal_01x01 J4
U 1 1 5DC1C079
P 4600 3200
F 0 "J4" H 4680 3242 50  0000 L CNN
F 1 "Screw_Terminal_01x01" H 4680 3151 50  0000 L CNN
F 2 "lsf-kicad-lib:KEYSTONE-7698" H 4600 3200 50  0001 C CNN
F 3 "~" H 4600 3200 50  0001 C CNN
	1    4600 3200
	-1   0    0    1   
$EndComp
$Comp
L Connector:Screw_Terminal_01x01 J3
U 1 1 5DC1C07F
P 4600 2850
F 0 "J3" H 4680 2892 50  0000 L CNN
F 1 "Screw_Terminal_01x01" H 4680 2801 50  0000 L CNN
F 2 "lsf-kicad-lib:KEYSTONE-7698" H 4600 2850 50  0001 C CNN
F 3 "~" H 4600 2850 50  0001 C CNN
	1    4600 2850
	-1   0    0    1   
$EndComp
Wire Wire Line
	4800 2850 5250 2850
Wire Wire Line
	4800 3200 5250 3200
Text Label 5250 2850 2    50   ~ 0
V_DEPLOY
Text Label 5250 3200 2    50   ~ 0
GND_D
$EndSCHEMATC
